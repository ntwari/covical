

## COVICAL

## Table of content

1.  [About The Project](https://github.com/ntwari-egide/New-readme-design/edit/master/README.md#about-the-project)
    -   [Built With](https://github.com/ntwari-egide/New-readme-design/edit/master/README.md#built-with)
2.  [Getting Started](https://github.com/ntwari-egide/New-readme-design/edit/master/README.md#getting-started)
    -   [Prerequisites](https://github.com/ntwari-egide/New-readme-design/edit/master/README.md#prerequisites)
    -   [Installation](https://github.com/ntwari-egide/New-readme-design/edit/master/README.md#installation)
3.  [Usage](https://github.com/ntwari-egide/New-readme-design/edit/master/README.md#usage)
4.  [Roadmap](https://github.com/ntwari-egide/New-readme-design/edit/master/README.md#roadmap)
5.  [Contributing](https://github.com/ntwari-egide/New-readme-design/edit/master/README.md#contributing)
6.  [License](https://github.com/ntwari-egide/New-readme-design/edit/master/README.md#license)
7.  [Contact](https://github.com/ntwari-egide/New-readme-design/edit/master/README.md#contact)

## About The Project

Welcome page of covical
### Filtering data by state

![enter image description here](https://res.cloudinary.com/dpqasrwfu/image/upload/v1631022836/new_changes_ivhjq3.png)

### Filtering data by country

![enter image description here](https://res.cloudinary.com/dpqasrwfu/image/upload/v1631094626/new_file1_rw3hdm.png)

### Footer of COVICAL

![enter image description here](https://res.cloudinary.com/dpqasrwfu/image/upload/v1631094629/other_uymr0f.png)

#### Build with

 - React Js (reusable components)
 - React Hooks
 - public [API](https://documenter.getpostman.com/view/11144369/Szf6Z9B3?version=latest)
 - Tailwind css

## Getting Started

This is an example of how you may give instructions on setting up your project locally. To get a local copy up and running follow these simple example steps.

### Prerequisites

This is an example of how to list things you need to use the software and how to install them.

 1.   Clone project from gitlab
- Gitlab link: https://gitlab.com/ntwari/covical
 
 2. Switching to your folder
 - After cloning,cd in folder called **covical**

### Installation
- npm
`npm install ` or  `yarn install`

## Usage
This systm shows COVID19 updates accross the world, filter by US state or any country, choose date, then press **submit button**

You can also know the data accros the continent like Asia, Europe, Africa, US, Australia

## Roadmap

See the  [user stories on our JIRA board]()  for a list of proposed features (and known issues).
## Contributing

Contributions are what make the this COVICAL open source project such an amazing, fantastic, inspire, and create new ideas. Any contributions you make are  **greatly appreciated**.

1.  Fork the Project from our gitlab https://gitlab.com/ntwari/covical
2.  Create your Feature Branch (`git checkout -b ft-AmazingFeature-userstories_id`)
3.  Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4.  Push to the Branch (`git push origin ft-AmazingFeature-userstories_id`)
5.  Open a Pull Request
6. Add yourself as assignee
7. Add ntwariegide2@gmail.com as reviewer

## License

Distributed under the MIT License. See  `LICENSE`  for more information.

## Contact

Egide Ntwari -  [egide2020](https://twitter.com/egide2020)  -  [ntwariegide2@gmail.com](mailto:ntwariegide2@gmail.com)

Project Link:  [COVICAL gitlab link]https://gitlab.com/ntwari/covical)